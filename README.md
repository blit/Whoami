# About me

Hi, I'm beginner sysadmin/programmer.

My Keybase: [@blitdev](https://keybase.io/blitdev)

Email (1): blitdev@disroot.org <br>
Email (2): blitdev@macaw.me

## My site

**[blit.cabal.run](https://blit.cabal.run)** (**_Clearnet_**)

## My GnuPG Key

**[Public key (it's OpenPGP compatible as I see.)](./gpg_key.asc)**

## My SSH Key

**[Public key](./ssh_key.pub)**

## I Learn

- **C#** and **Python** (programming languages)
- **Bash?**

## Messengers

**Telegram**: @blitdev (but don't use...) <br>
**Session ID**: 059da0efc7f50d6793f7bc88361ee67f6f27acec7043b74fbb282d4bf96f3d5768 <br>
**Matrix**: @blitdev:inex.rocks (I prefer Nheko) <br>
**XMPP/Jabber**: blitdev@disroot.org

## My Fediverse accounts

### Social

- [blitdev@fe.disroot.org](https://fe.disroot.org/@blitdev) (Akkoma, Mangane) (main)
- [blitdev@lamp.leemoon.network](https://lamp.leemoon.network/@blitdev) (Firefish) (sometimes check)
- [blitdev@mastodon.ml](https://mastodon.ml/@blitdev) (Mastodon) (inactive)
- [blitdev@udongein.xyz](https://udongein.xyz/blitdev) (Pleroma, Pleroma FE) (inactive)

### PeerTube

- [blitdev@peervideo.ru](https://peervideo.ru/a/blitdev/video-channels) (main)
- [blitdev@peertube.su](https://peertube.su/a/blitdev/video-channels) (main too)
